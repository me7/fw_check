import os

class Config:
	def __init__(self,path='app/config'):
		self.config = {}
		config_dir = os.path.abspath(path)
		for f in os.listdir(config_dir):
			if f.endswith('.ini'):
				group = f[:-4]
				filename = os.path.abspath(os.path.join(config_dir,f))
				with open(filename) as f:
					for line in f.readlines():
						l = line.strip().split('=')
						if group == 'global':
							self.config[l[0]] = l[1]
						else:
							self.config[group+'.'+l[0]] = l[1]
	
	def get(self, key, default=None):
		try:
			return self.config[key]
		except KeyError:
			return default
		
	def set(self, key):
		raise NotImplementedError, "Please edit INI file directly"
		
	def __str__(self):
		s = ''
		for k,v in self.config.items():
			s += k+'='+v+'\n'
		return s

if __name__ == "__main__":
	config = Config('./config')
else:
	config = Config()